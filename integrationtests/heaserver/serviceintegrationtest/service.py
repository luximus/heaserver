"""
Test HEA service.
"""

from heaserver.service import response
from heaserver.service.appproperty import HEA_DB
from heaserver.service.runner import routes, web
from heaserver.service.db import mongoservicelib
from heaserver.service.wstl import action
from heaobject.registry import Component

MONGODB_COMPONENT_COLLECTION = 'components'


@routes.get('/components/{id}')
@action('component-get-properties', rel='properties')
@action('component-open', rel='opener', path='/components/{id}/opener', include_root=True)  # only add if the object is openable
@action('component-duplicate', rel='duplicator', path='/components/{id}/duplicator', include_root=True)
async def get_component(request: web.Request) -> web.Response:
    """
    Gets the component with the specified id.
    :param request: the HTTP request.
    :return: the requested component or Not Found.
    """
    return await mongoservicelib.get(request, MONGODB_COMPONENT_COLLECTION)


@routes.get('/components/byname/{name}')
async def get_component_by_name(request: web.Request) -> web.Response:
    """
    Gets the component with the specified name.
    :param request: the HTTP request.
    :return: the requested component or Not Found.
    """
    return await mongoservicelib.get_by_name(request, MONGODB_COMPONENT_COLLECTION)


@routes.get('/components/bytype/{type}')
async def get_components_by_type(request: web.Request) -> web.Response:
    """
    Gets the component that serves resources of the specified HEA object type.
    :param request: the HTTP request.
    :return: the requested component or Not Found.
    """
    result = await request.app[HEA_DB].get(request,
                                           MONGODB_COMPONENT_COLLECTION,
                                           mongoattributes={'resources': {
                                               '$elemMatch': {
                                                   'resource_type_name': {'$in': [request.match_info['type']]}}}})
    return await response.get(request, result)


@routes.get('/components')
@routes.get('/components/')
@action('component-get-properties', rel='properties')
@action('component-open', rel='opener', path='/components/{id}/opener', include_root=True)  # only add if the object is openable
@action('component-duplicate', rel='duplicator', path='/components/{id}/duplicator', include_root=True)
async def get_all_components(request: web.Request) -> web.Response:
    """
    Gets all components.
    :param request: the HTTP request.
    :return: all components.
    """
    return await mongoservicelib.get_all(request, MONGODB_COMPONENT_COLLECTION)


@routes.get('/components/{id}/duplicator')
@action(name='component-duplicate-form')
async def get_component_duplicator(request: web.Request) -> web.Response:
    """
    Gets a form template for duplicating the requested component.

    :param request: the HTTP request. Required.
    :return: the requested form, or Not Found if the requested component was not found.
    """
    return await mongoservicelib.get(request, MONGODB_COMPONENT_COLLECTION)


@routes.post('/components/duplicator')
async def post_component_duplicator(request: web.Request) -> web.Response:
    """
    Posts the provided component for duplication.
    :param request: the HTTP request.
    :return: a Response object with a status of Created and the object's URI in the
    """
    return await mongoservicelib.post(request, MONGODB_COMPONENT_COLLECTION, Component)


@routes.get('/components/{id}/opener')
async def get_component_opener(request: web.Request) -> web.Response:
    """
    Opens the component. We wouldn't ordinarily open a component because there's nothing to open.

    :param request: the HTTP request. Required.
    :return: the requested form, or Not Found if the requested component was not found.
    """
    if await mongoservicelib.get(request, MONGODB_COMPONENT_COLLECTION) is None:  # User may not have access
        return response.status_not_found()
    else:
        return response.status_multiple_choices(request.url.parent)


@routes.post('/components')
@routes.post('/components/')
async def post_component(request: web.Request) -> web.Response:
    """
    Posts the provided component.
    :param request: the HTTP request.
    :return: a Response object with a status of Created and the object's URI in the
    """
    return await mongoservicelib.post(request, MONGODB_COMPONENT_COLLECTION, Component)


@routes.put('/components/{id}')
async def put_component(request: web.Request) -> web.Response:
    """
    Updates the component with the specified id.
    :param request: the HTTP request.
    :return: a Response object with a status of No Content or Not Found.
    """
    return await mongoservicelib.put(request, MONGODB_COMPONENT_COLLECTION, Component)


@routes.delete('/components/{id}')
async def delete_component(request: web.Request) -> web.Response:
    """
    Deletes the component with the specified id.
    :param request: the HTTP request.
    :return: No Content or Not Found.
    """
    return await mongoservicelib.delete(request, MONGODB_COMPONENT_COLLECTION)
