from aiohttp.test_utils import unittest_run_loop
from .componenttestcase import ComponentTestCase
from heaserver.service.testcase.mixin import PutMixin


class TestPutComponent(ComponentTestCase, PutMixin):  # type: ignore

    @unittest_run_loop
    async def test_put_status_invalid_base_url(self):
        await self._test_invalid({'base_url': 2})

    @unittest_run_loop
    async def test_put_status_invalid_resource(self):
        await self._test_invalid({'resources': [2]})



