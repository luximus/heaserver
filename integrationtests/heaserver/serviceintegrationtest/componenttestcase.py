from . import service
from heaserver.service.testcase.integrationmongotestcase import get_test_case_cls_default
from heaserver.service.testcase.expectedvalues import ActionSpec
from typing import Dict, List, Any

fixtures: Dict[str, List[Dict[str, Any]]] = {
    service.MONGODB_COMPONENT_COLLECTION: [{
        'id': '666f6f2d6261722d71757578',
        'created': None,
        'derived_by': None,
        'derived_from': [],
        'description': None,
        'display_name': 'Reximus',
        'invited': [],
        'modified': None,
        'name': 'reximus',
        'owner': 'system|none',
        'shared_with': [],
        'source': None,
        'type': 'heaobject.registry.Component',
        'version': None,
        'base_url': 'http://localhost/foo',
        'resources': []
    },
        {
            'id': '0123456789ab0123456789ab',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': None,
            'display_name': 'Luximus',
            'invited': [],
            'modified': None,
            'name': 'luximus',
            'owner': 'system|none',
            'shared_with': [],
            'source': None,
            'type': 'heaobject.registry.Component',
            'version': None,
            'base_url': 'http://localhost/foo',
            'resources': []
        }
    ]}

ComponentTestCase = get_test_case_cls_default(coll=service.MONGODB_COMPONENT_COLLECTION,
                                              href='http://localhost:8080/components/',
                                              wstl_package=service.__package__,
                                              fixtures=fixtures,
                                              get_actions=[ActionSpec(name='component-get-properties',
                                                                      rel=['properties']),
                                                           ActionSpec(name='component-open',
                                                                      url='http://localhost:8080/components/{id}/opener',
                                                                      rel=['opener']),
                                                           ActionSpec(name='component-duplicate',
                                                                      url='http://localhost:8080/components/{id}/duplicator',
                                                                      rel=['duplicator'])],
                                              get_all_actions=[ActionSpec(name='component-get-properties',
                                                                          rel=['properties']),
                                                               ActionSpec(name='component-open',
                                                                          url='http://localhost:8080/components/{id}/opener',
                                                                          rel=['opener']),
                                                               ActionSpec(name='component-duplicate',
                                                                          url='http://localhost:8080/components/{id}/duplicator',
                                                                          rel=['duplicator'])],
                                              expected_opener=f'http://localhost:8080/components/{fixtures[service.MONGODB_COMPONENT_COLLECTION][0]["id"]}',
                                              duplicate_action_name='component-duplicate-form',
                                              include_root=True)
