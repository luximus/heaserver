class RepresentorException(Exception):
    pass


class ParseException(RepresentorException):
    pass


class FormatException(RepresentorException):
    pass
