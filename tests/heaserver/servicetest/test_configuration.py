import unittest
from heaserver.service import runner


class TestConfiguration(unittest.TestCase):

    def setUp(self) -> None:
        self.config = runner.init_cmd_line(description='foo')

    def test_default_base_url(self) -> None:
        self.assertEqual(runner.DEFAULT_BASE_URL, self.config.base_url)


