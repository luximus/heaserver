from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web
from heaserver.service import appproperty, runner, wstl
from heaserver.service.db.mockmongo import MockMongo
from heaserver.service.heaobjectsupport import populate_heaobject, new_heaobject, type_to_resource_url
from heaobject.folder import Folder
from heaobject import user
from . import service


class TestHEAObjectSupport(AioHTTPTestCase):

    async def setUpAsync(self):
        self.__body = {
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': None,
            'display_name': 'Reximus',
            'id': None,
            'invites': [],
            'modified': None,
            'name': 'reximus',
            'owner': user.NONE_USER,
            'shares': [],
            'source': None,
            'type': 'heaobject.folder.Folder',
            'version': None,
            'mime_type': 'application/x.folder'
        }

    async def get_application(self):
        async def test_new_heaobject(request):
            obj = await new_heaobject(request, Folder)
            return web.Response(status=200, body=obj.json_dumps(), content_type='application/json')

        async def test_populate_heaobject(request):
            obj = await populate_heaobject(request, Folder())
            return web.Response(status=200, body=obj.json_dumps(), content_type='application/json')

        async def test_type_to_resource_url(request):
            app[appproperty.HEA_REGISTRY] = 'http://127.0.0.1:' + str(request.url.port)
            obj = await type_to_resource_url(request, Folder)
            return web.Response(status=200, body=(obj if obj is None else obj.json_dumps()),
                                content_type='application/json')

        app = runner.get_application(db=MockMongo, wstl_builder_factory=wstl.builder_factory(package=service.__package__))
        app.router.add_post('/testnewheaobject', test_new_heaobject)
        app.router.add_post('/testpopulateheaobject', test_populate_heaobject)
        app.router.add_get('/testtypetoresourceurl', test_type_to_resource_url)

        return app

    @unittest_run_loop
    async def test_new_heaobject(self):
        obj = await self.client.request('POST', '/testnewheaobject', json=self.__body)
        self.assertEqual(self.__body, await obj.json())

    @unittest_run_loop
    async def test_populate_heaobject(self):
        obj = await self.client.request('POST', '/testpopulateheaobject', json=self.__body)
        self.assertEqual(self.__body, await obj.json())

    @unittest_run_loop
    async def test_type_to_resource_url_status(self):
        response = await self.client.request('GET', '/testtypetoresourceurl')
        self.assertEqual(200, response.status)

    @unittest_run_loop
    async def test_type_to_resource_url_none(self):
        response = await self.client.request('GET', '/testtypetoresourceurl')
        self.assertEqual(None, await response.json())
