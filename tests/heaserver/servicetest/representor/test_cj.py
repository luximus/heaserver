from heaserver.service.testcase.simpleaiohttptestcase import SimpleAioHTTPTestCase, unittest_run_loop
import json
from heaserver.service import wstl
from heaserver.service.representor import cj, error
from heaobject import user
from unittest import mock


class TestCj(SimpleAioHTTPTestCase):

    async def setUpAsync(self):
        self.__body = {
            'items': [],
            'created': None,
            'derived_by_uri': None,
            'derived_from_uris': [],
            'description': None,
            'display_name': 'Reximus',
            'id': '1',
            'invites': [],
            'modified': None,
            'name': 'reximus',
            'owner': user.NONE_USER,
            'shares': [],
            'source_uri': None,
            'type': 'heaobject.folder.Folder',
            'version': None
        }
        self.maxDiff = None

    class __MockRequest:
        def __init__(self, jsons='{}'):
            self.jsons = jsons
            self.match_info = {}

        async def text(self):
            return self.jsons

        async def json(self, loads=json.loads):
            return loads(self.jsons)

    @unittest_run_loop
    async def test_formats_default(self):
        b = wstl.builder('servicetest', resource='representor/all.json')
        actual = await cj.CJ().formats(TestCj.__MockRequest(), b())
        expected = '[{"collection": {"href": "#", "version": "1.0"}}]'
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection":
                          {"version": "1.0",
                           "href": "http://localhost/test",
                           "items": [
                               {"data": [
                                   {"name": "items", "value": [], "prompt": "items", "display": true},
                                   {"name": "created", "value": null, "prompt": "created", "display": true},
                                   {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                   {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                   {"name": "description", "value": null, "prompt": "description", "display": true},
                                   {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                   {"name": "id", "value": "1", "prompt": "id", "display": false},
                                   {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                   {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                   {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                   {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                   {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                   {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                   {"name": "version", "value": null, "prompt": "version", "display": true}
                                   ],
                                   "links": []
                               }
                           ]}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data_with_item_href(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all_cj_item_href.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl_builder.add_run_time_action('data-adapter-open', path='/foo/bar/{id}', root='http://localhost:8080')
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection":
                         {"version": "1.0",
                          "href": "http://localhost/test",
                          "items": [
                              {"data": [
                                  {"name": "items", "value": [], "prompt": "items", "display": true},
                                  {"name": "created", "value": null, "prompt": "created", "display": true},
                                  {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                  {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                  {"name": "description", "value": null, "prompt": "description", "display": true},
                                  {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                  {"name": "id", "value": "1", "prompt": "id", "display": false},
                                  {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                  {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                  {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                  {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                  {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                  {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                  {"name": "version", "value": null, "prompt": "version", "display": true}
                                  ],
                                  "rel": "",
                                  "href": "http://localhost:8080/foo/bar/1",
                                  "links": []
                              }
                          ]}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data_with_item_link(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all_cj_item_link.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl_builder.add_run_time_action('data-adapter-open', path='/foo/bar/{id}', root='http://localhost:8080')
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection":
                          {"version": "1.0",
                           "href": "http://localhost/test",
                           "items": [
                               {"data": [
                                   {"name": "items", "value": [], "prompt": "items", "display": true},
                                   {"name": "created", "value": null, "prompt": "created", "display": true},
                                   {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                   {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                   {"name": "description", "value": null, "prompt": "description", "display": true},
                                   {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                   {"name": "id", "value": "1", "prompt": "id", "display": false},
                                   {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                   {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                   {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                   {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                   {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                   {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                   {"name": "version", "value": null, "prompt": "version", "display": true}
                                ],
                                "links": [
                                    {"prompt": "Open", "rel": "", "href": "http://localhost:8080/foo/bar/1"}
                                ]
                           }]}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data_with_toplevel_link_for_item(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all_cj_toplevel_link_for_item.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl_builder.add_run_time_action('data-adapter-open', path='/foo/bar', root='http://localhost:8080')
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection": {
                          "version": "1.0",
                          "href": "http://localhost/test",
                          "items": [
                              {"data": [
                                  {"name": "items", "value": [], "prompt": "items", "display": true},
                                  {"name": "created", "value": null, "prompt": "created", "display": true},
                                  {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                  {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                  {"name": "description", "value": null, "prompt": "description", "display": true},
                                  {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                  {"name": "id", "value": "1", "prompt": "id", "display": false},
                                  {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                  {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                  {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                  {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                  {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                  {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                  {"name": "version", "value": null, "prompt": "version", "display": true}
                               ],
                               "links": []
                               }
                          ],
                          "links": [
                              {"prompt": "Open", "rel": "", "href": "http://localhost:8080/foo/bar"}
                          ]}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data_with_toplevel_link_for_list(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all_cj_toplevel_link_for_list.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl_builder.add_run_time_action('data-adapter-list', path='/foo/bar', root='http://localhost/test')
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection": {
                                  "version": "1.0",
                                  "href": "http://localhost/test",
                                  "items": [
                                      {"data": [
                                          {"name": "items", "value": [], "prompt": "items", "display": true},
                                          {"name": "created", "value": null, "prompt": "created", "display": true},
                                          {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                          {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                          {"name": "description", "value": null, "prompt": "description", "display": true},
                                          {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                          {"name": "id", "value": "1", "prompt": "id", "display": false},
                                          {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                          {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                          {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                          {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                          {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                          {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                          {"name": "version", "value": null, "prompt": "version", "display": true}
                                       ],
                                       "links": []
                                       }
                                  ],
                                  "links": [
                                      {"prompt": "List", "rel": "", "href": "http://localhost/test/foo/bar"}
                                  ]}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data_with_queries(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all_cj_queries.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl_builder.add_run_time_action('data-adapter-search', path='/foo/bar', root='http://localhost/test')
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection": {
                              "version": "1.0",
                              "href": "http://localhost/test",
                              "items": [
                                  {"data": [
                                      {"name": "items", "value": [], "prompt": "items", "display": true},
                                      {"name": "created", "value": null, "prompt": "created", "display": true},
                                      {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                      {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                      {"name": "description", "value": null, "prompt": "description", "display": true},
                                      {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                      {"name": "id", "value": "1", "prompt": "id", "display": false},
                                      {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                      {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                      {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                      {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                      {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                      {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                      {"name": "version", "value": null, "prompt": "version", "display": true}
                                   ],
                                   "links": []
                                   }
                              ],
                              "queries": [{
                                  "data": [{
                                      "name": "keywords",
                                      "pattern": "",
                                      "prompt": "Keywords",
                                      "readOnly": false,
                                      "required": true,
                                      "value": ""}, {
                                      "name": "match-case",
                                      "pattern": "",
                                      "prompt": "Match case?",
                                      "readOnly": false,
                                      "required": true,
                                      "value": ""}],
                                  "href": "http://localhost/test/foo/bar",
                                  "prompt": "Search",
                                  "rel": ""}]}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_formats_data_with_template(self):
        runtime_wstl_builder = wstl.builder('servicetest.representor', resource='all_cj_template.json')
        runtime_wstl_builder.data = self.__body
        runtime_wstl_builder.href = 'http://localhost/test'
        runtime_wstl_builder.add_run_time_action('data-adapter-search', path='/foo/bar', root='http://localhost/test')
        runtime_wstl = runtime_wstl_builder()
        actual = await cj.CJ().formats(TestCj.__MockRequest(), runtime_wstl)
        expected = '''[{"collection": {
                                  "version": "1.0",
                                  "href": "http://localhost/test",
                                  "items": [
                                      {"data": [
                                          {"name": "items", "value": [], "prompt": "items", "display": true},
                                          {"name": "created", "value": null, "prompt": "created", "display": true},
                                          {"name": "derived_by_uri", "value": null, "prompt": "derived_by_uri", "display": true},
                                          {"name": "derived_from_uris", "value": [], "prompt": "derived_from_uris", "display": true},
                                          {"name": "description", "value": null, "prompt": "description", "display": true},
                                          {"name": "display_name", "value": "Reximus", "prompt": "display_name", "display": true},
                                          {"name": "id", "value": "1", "prompt": "id", "display": false},
                                          {"name": "invites", "value": [], "prompt": "invites", "display": true},
                                          {"name": "modified", "value": null, "prompt": "modified", "display": true},
                                          {"name": "name", "value": "reximus", "prompt": "name", "display": true},
                                          {"name": "owner", "value": "system|none", "prompt": "owner", "display": true},
                                          {"name": "shares", "value": [], "prompt": "shares", "display": true},
                                          {"name": "source_uri", "value": null, "prompt": "source_uri", "display": true},
                                          {"name": "version", "value": null, "prompt": "version", "display": true}
                                       ],
                                       "links": []
                                       }
                                  ],
                                  "template": {
                                      "data": [
                                          {"name": "keywords",
                                           "pattern": "",
                                           "prompt": "Keywords",
                                           "readOnly": false,
                                           "required": true,
                                           "value": null},
                                          {"name": "match-case",
                                           "pattern": "",
                                           "prompt": "Match case?",
                                           "readOnly": false,
                                           "required": true,
                                           "value": null}],
                                      "prompt": "Search",
                                      "rel": ""}}}]'''
        self._assert_json_string_equals(expected, actual)

    @unittest_run_loop
    async def test_parses_template(self):
        cj_str = '{"template": {"data": [{"name": "foo", "value": "bar"}, {"name": "baz", "value": "oof"}]}}'
        actual = await cj.CJ().parse(TestCj.__MockRequest(cj_str))
        expected = {"foo": "bar", "baz": "oof"}
        self.assertEqual(expected, actual)

    @unittest_run_loop
    async def test_parses_missing_data_object(self):
        cj_str = '{"template": {}}'
        actual = await cj.CJ().parse(TestCj.__MockRequest(cj_str))
        expected = {}
        self.assertEqual(expected, actual)

    @unittest_run_loop
    async def test_parses_empty_data_array(self):
        cj_str = '{"template": {"data": []}}'
        actual = await cj.CJ().parse(TestCj.__MockRequest(cj_str))
        expected = {}
        self.assertEqual(expected, actual)

    @unittest_run_loop
    async def test_parses_missing_template_property(self):
        with self.assertRaises(error.ParseException):
            await cj.CJ().parse(TestCj.__MockRequest('{"tmplte": {"data": []}}'))

    @unittest_run_loop
    async def test_parses_empty(self):
        with self.assertRaises(error.ParseException):
            await cj.CJ().parse(TestCj.__MockRequest('{}'))

    @unittest_run_loop
    async def test_parses_no_name(self):
        with self.assertRaises(error.ParseException):
            await cj.CJ().parse(TestCj.__MockRequest('{"template": {"data": [{"nm": "foo"}]}}'))

    @unittest_run_loop
    async def test_parses_no_list(self):
        with self.assertRaises(error.ParseException):
            await cj.CJ().parse(TestCj.__MockRequest('{"template": {"data": {"name": "foo"}}}'))
